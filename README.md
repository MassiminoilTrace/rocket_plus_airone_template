A template of a [rocket](https://rocket.rs/) server which
- integrates an example [Airone](https://gitlab.com/MassiminoilTrace/airone) database
- serves static files in debug mode to ease development, but not in release mode so you can leverage existing NGINX/Apache webservers, similar to Djang's `runserver` option

# Usage

Launch with `cargo run` or `cargo run --release`

Play with the webpage by adding some data. Every changed will be saved in a `Farm.changes.csv` file in the project folder.

Then stop the server, run it again. The previous changes are correctly preserved, the `Farm.changes.csv` file has been emptied by writing the resulting data to the `Farm.csv` base file.

# Copyright
This is **NOT** public domain, make sure to respect the license terms.
You can find the license text in the [COPYING](https://gitlab.com/MassiminoilTrace/airone/-/blob/master/COPYING) file.

Copyright © 2022,2023 Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.


## External libraries
This template uses libraries licensed in various ways, including AGPL3+ one. Make sure to check the correct license compatibility with your own use.

For copyright reference, read the libraries included in the Cargo.toml file.

This sample project includes the MIT(expat)-licensed [simple.css](https://github.com/kevquirk/simple.css/) project, as an example CSS styling. Its minified file is included at `/static/simple.min.css`.
