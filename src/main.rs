#[macro_use] extern crate rocket;

use std::path::{Path, PathBuf};
use rocket::http::Status;
use rocket::response::Redirect;
use rocket::serde::Serialize;
use rocket::{fs::NamedFile, form::Form};
use rocket::State;

use rocket::tokio::sync::RwLock;

use rocket_dyn_templates::{Template, context};


// Include airone library
use airone::prelude::*;

#[derive(AironeDbDerive)]
#[derive(FromForm)]
#[derive(Serialize)]
#[serde(crate="rocket::serde")]
pub struct Farm
{
    name: String,
    address: String,
    number_of_cows: u32
}


/// Display the number of current farms
#[get("/")]
async fn index_template(db: &State<RwLock<AironeDb<Farm>>>) -> Template {
    let r_guard = db.read().await;
    return Template::render("index", context!{
        farms: r_guard.get_all()
    })
}


#[post("/add_farm", data="<new_farm>")]
async fn add_farm(
    lock: &State<RwLock<AironeDb<Farm>>>,
    new_farm: Form<Farm>
) -> Result<Redirect, Status>
{
    let mut db_guard = lock.write().await;
    db_guard.push(
        new_farm.into_inner()
    ).map_err(|_|Status::InternalServerError)?;
    
    return Ok(
        Redirect::to(
            uri!(index_template)
        )
    )
}




// Defined only in Debug mode.
// It serves static files from Rocket to ease development,
// in a similar fashion to Django's "runserver" command
#[cfg(debug_assertions)]
#[get("/static/<file..>")]
async fn serve_static_file(file: PathBuf) -> Option<NamedFile>
{
    NamedFile::open(Path::new("static/").join(&file)).await.ok()
}

#[launch]
fn rocket() -> _ {
    // Mounting main routes
    let mut rocket_builder = rocket::build()
    .attach(Template::fairing())
    .manage(
        RwLock::new(
            AironeDb::<Farm>::new()
        )
    )
    .mount("/", routes![index_template, add_farm]);


    // Mounting also a route for static files
    // in DEBUG mode ONLY.
    // Static files will be served using NGINX or an external program
    // to achieve better performance and cache control.
    #[cfg(debug_assertions)]
    {
        rocket_builder = rocket_builder.mount("/", routes![serve_static_file]);
    }

    // Returning the rocket object to start the server
    rocket_builder
}
